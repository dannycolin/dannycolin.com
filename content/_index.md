---
title: "About Me"
---

Hello there! My name is Danny. I’m a student in anthropology at the
[University of Montreal][umontreal] and a web developer. I’m also an open source,
open science and ecologist activist.

Here’s a list of projects and organizations I’m involved in:

- [Koumbit][koumbit]
- [Open Science UMontreal][osum]
- [Support Mozilla][sumo]

You can follow me on 
[Twitter][twitter], 
<a rel="me" href="https://fosstodon.org/@dannycolin">Mastodon</a>,
[GitHub][github]
and [GitLab][gitlab].
If you prefer, you can also [email me][email].

[email]: mailto:ping@dannycolin.com
[github]: https://github.com/dannycolin
[gitlab]: https://gitlab.com/dannycolin
[koumbit]: https://koumbit.org/en
[mastodon]: https://fosstodon.org/@dannycolin
[osmooc]: https://opensciencemooc.eu
[osum]: https://osumontreal.ca
[rqge]: https://rqge.qc.ca
[sumo]: https://support.mozilla.org
[twitter]: https://twitter.com/dannycolincom
[umontreal]: http://www.umontreal.ca/en
