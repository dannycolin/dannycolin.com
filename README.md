# dannycolin.com

Git repository for my personal website.

## License
The source code for this site is licensed under [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.txt). Unless otherwise stated, the content of this site is licensed under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

## Webhosting
This website is hosted at [Koumbit](https://koumbit.org/en) which uses only free software on their servers.
